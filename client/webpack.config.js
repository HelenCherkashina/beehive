var webpack = require('webpack');

var path = require('path');
var packageJSON = require('./package.json');

var ENV = process.env.ENV = process.env.NODE_ENV = 'development';

const paths = {
    app: path.join(__dirname, '/src/app'),
    build: path.join(__dirname, 'target', 'classes', 'META-INF', 'resources', 'js', packageJSON.name, packageJSON.version)
};

module.exports = {
    context: paths.app,
    node: {
        __dirname: "mock"
    },
    entry: {
        main: './main.ts'
    },
    output: {
        path: paths.build,
        filename: "[name].bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: "babel-loader"
            },
            {
                test: /\.tsx?$/, 
                loader: 'ts-loader',
                exclude: [/\.(spec|e2e|async)\.ts$/]
            }
        ]
    },
    devServer: {
        contentBase: paths.build,
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        stats: 'errors-only',
        host: 'localhost',
        port: 3000,
        proxy: {
            '/*': {
                target: 'http://localhost:8080',
                secure: false,
                prependPath: false
            },
            '/login/*': {
                target: 'http://localhost:8082',
                secure: true,
                prependPath: false
            }
        }
    },
    devtool: 'source-map'
}