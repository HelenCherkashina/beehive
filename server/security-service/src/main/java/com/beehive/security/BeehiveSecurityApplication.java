package com.beehive.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@SpringBootApplication
@EnableAutoConfiguration
public class BeehiveSecurityApplication {
	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json")
	public String user(Principal principal) {
		return principal.getName();
	}

	public static void main(String[] args) {
		SpringApplication.run(BeehiveSecurityApplication.class, args);
	}
}
