package com.beehive.security.config.providers;

import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;

import javax.annotation.Resource;
import javax.servlet.Filter;

public abstract class OAuthProviderConfig {
    OAuth2ProtectedResourceDetails client = new AuthorizationCodeResourceDetails();
    ResourceServerProperties resource = new ResourceServerProperties();

    @Resource
    private OAuth2ClientContext oauth2ClientContext;

    abstract OAuth2ProtectedResourceDetails getClient();
    abstract ResourceServerProperties getResource();
    abstract String getClientUrl();

    Filter getFilter() {
        OAuth2ClientAuthenticationProcessingFilter filter =
                new OAuth2ClientAuthenticationProcessingFilter(getClientUrl());
        OAuth2RestTemplate template = new OAuth2RestTemplate(getClient(), oauth2ClientContext);
        filter.setRestTemplate(template);
        filter.setTokenServices(new UserInfoTokenServices(getResource().getUserInfoUri(),
                getClient().getClientId()));
        return filter;
    }
}
