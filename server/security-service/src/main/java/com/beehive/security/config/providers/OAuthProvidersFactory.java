package com.beehive.security.config.providers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CompositeFilter;

import javax.servlet.Filter;
import java.util.Arrays;

@Component
public class OAuthProvidersFactory {
    @Autowired
    private OAuthFacebookProviderConfig facebook;

    public OAuthFacebookProviderConfig getProvider(String providerName) {
        switch (providerName) {
            case "facebook": return facebook;
            default: throw new IllegalArgumentException(String.format("Can't find provider with %s name", providerName));
        }
    }

    @Bean
    public Filter getAllProviders() {
        CompositeFilter filter = new CompositeFilter();
        filter.setFilters(Arrays.asList(facebook.getFilter()));
        return filter;
    }
}
