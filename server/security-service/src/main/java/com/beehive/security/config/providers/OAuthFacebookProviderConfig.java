package com.beehive.security.config.providers;

import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;

@Configuration
public class OAuthFacebookProviderConfig extends OAuthProviderConfig {
    @Bean
    @ConfigurationProperties("facebook.client")
    OAuth2ProtectedResourceDetails getClient() {
        return client;
    }

    @Bean
    @ConfigurationProperties("facebook.resource")
    ResourceServerProperties getResource() {
        return resource;
    }

    @Override
    protected String getClientUrl() {
        return "/login/facebook";
    }
}
