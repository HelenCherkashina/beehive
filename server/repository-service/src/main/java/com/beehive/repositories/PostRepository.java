package com.beehive.repositories;

import com.beehive.models.Post;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PostRepository extends MongoRepository<Post, ObjectId>, PagingAndSortingRepository<Post, ObjectId> {
}
