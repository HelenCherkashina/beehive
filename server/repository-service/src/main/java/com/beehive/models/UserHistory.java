package com.beehive.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@NoArgsConstructor
public class UserHistory {
    @Id
    private ObjectId id;
    private Date dateOfRegistration;
    private Integer postsCount;
    private Integer commentsCount;
    private Integer rating;
}
