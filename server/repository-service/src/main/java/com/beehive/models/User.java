package com.beehive.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.management.relation.Role;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Document(collection = "users")
public class User implements Serializable {
    @Id
    private ObjectId id;
    private String login;
    private String password;
    private String email;
    private Date birthdate;
    private Role role;
    @DBRef
    private UserHistory userHistory;
    private List<Post> posts;
}
