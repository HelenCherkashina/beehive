package com.beehive.models;

import com.beehive.enums.PostType;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Document(collection = "posts")
public class Post implements Serializable {
    @Id
    private ObjectId id;
    private String message;
    private Integer commentsCount;
    private PostType type;
    private Integer rating;
    private Date dateOfPublication;
    @DBRef
    private Post parentPost;
    private List<?> comments;
    private List<String> tags;
}
