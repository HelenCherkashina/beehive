package com.beehive.models;

import com.beehive.enums.PostType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class PostDto implements Serializable {
    private String id;
    private String message;
    private Integer commentsCount;
    private PostType type;
    private Integer rating;
    private Date dateOfPublication;
    private PostDto parentPostDto;
    private List<PostDto> comments;
    private List<String> tags;
}
