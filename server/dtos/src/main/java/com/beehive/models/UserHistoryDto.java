package com.beehive.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class UserHistoryDto {
    private String id;
    private Date dateOfRegistration;
    private Integer postsCount;
    private Integer commentsCount;
    private Integer rating;
}
