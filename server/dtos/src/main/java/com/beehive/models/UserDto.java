package com.beehive.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.management.relation.Role;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class UserDto implements Serializable {
    private String id;
    private String login;
    private String password;
    private String email;
    private Date birthdate;
    private Role role;
    private UserHistoryDto userHistoryDto;
    private List<PostDto> posts;
}
